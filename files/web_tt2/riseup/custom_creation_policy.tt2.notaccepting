
<p style="font-size:175%"><b>Unfortunately, we will temporarily not be accepting any more lists. We 
are at the capacity on our current server, and adding new lists would jeopardize our 
ability to provide reliable service to existing lists. As soon as we get things working
on a new server, we will begin accepting new lists. We anticipate this to 
be in April 2009. 
</b></p>
<p>If you want to see about getting a list hosted by another radical tech collective, 
we recommend our friends at <a href= "https://en.wiki.aktivix.org/Activix:EmailAndLists"> 
Aktivix</a>.</p>

<p>
By creating an email list at <b>lists.riseup.net</b> you agree to the following conditions:
<ul>

<li><b>Prior consent</b>: Every email address which you add to your list <b>must</b>
have given prior consent to be subscribed. Membership in an organization,
signing up online or at a meeting, or verbal approval all count as prior
consent. If in doubt, don't subscribe someone! If we receive complaints
about your list, it may be placed on probation or disabled.
</li>

<li><b>Political focus</b>: We only host lists which are primarily focused
on liberatory social change. The list must be progressive, radical, or
revolutionary in nature.
</li>

<li><b>Lists we don't host</b>: Here are some examples of list topics we decline
to host:
<ul>
  <li>hobbies: model trains, book groups, religious discussion, etc.</li>
  <li>university credited classes: please get your university or college to host the list, not us.</li>
  <li>commerce: no for-profit business related lists of any kind.</li>
  <li>personal: “me on vacation,” “updates about my life,” etc.</li>
  <li>capitalists: liberal causes, the Democratic party, etc.</li>
  <li>vanguard parties: ISO, RCP, SWP, and other authoritarian statist formations.</li>
  <li>cultural: upcoming performances, venues, etc.</li>
  <li>indymedia lists: we are trying to keep all indymedia lists on the indymedia server. go to <a href="http://newlist.indymedia.org/">newlist.indymedia.org</a> to request an indymedia list.</li>
</ul>
</li>

<li><b>Donations</b>: It takes a lot of time and money to provide this service. It
is funded entirely by donations from users like you. Unless you have no
income, or live in the global south, visit
<a href="https://riseup.net/donate">riseup.net/donate</a> and contribute accordingly.
</li>

<li><b>Admin responsibilities</b>: You, the list owner, are responsible for
unsubscribing people, moderating the list, removing bouncing subscribers,
closing unused lists, and responding to complaints regarding your list.
</li>

<li><b>Terms of Service</b>: By creating a list, you agree to riseup.net's
<a href="https://riseup.net/about-us/policy/tos/">Terms of Service Agreement</a>.
</li>

<li><b>Please don't use acronyms!</b>: Your list request will get rejected if
you use only acronyms. There are a bazillion groups out there and you cannot
assume that we know the acronym for yours.
</li>

<li><b>Please be patient</b>: We are a volunteer-run collective, and it can take a few days for us to approve requests. If your action is tomorrow, it is probably too late to create a list to publicize it.
</li>

</ul>













