Blacklist
-----------

We do not want the term blacklist to occur in the interface.
Where possible, this is replaced with 'blocklist'. However, these strings
has not been translated yet:

Blocklist

blocklist

The current blocklist is empty

The current blocklist contains %1 line(s)

Reject and blocklist sender

edit blocklist

<changeme>Operation requested by users which email is listed in the blacklist file are rejected. The
blacklist is in use for the following operation



Needs Translation
------------------

Contact list admins (was Contact owners)

Lists by topic

First login? (was First login ?)

Lost password? (was Lost password ?)

New email address (was New email address :)

New password (was New password :)

Re-enter your new password (was Re-enter your new password :)

Password (was Password :)

Other email address (was Other email address :)

Login expiration (was Connection expiration period)

Picture upload
--------------

either disable ui or fix picture upload.
