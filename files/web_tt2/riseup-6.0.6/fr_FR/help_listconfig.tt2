	<h4 class=block"><a name="listconfig"></a>Configurer la liste</h4>
	<p>La configuration de la liste étant assez complexe, elle se décompose en plusieurs volets correspondant chacun à une page différente&#160;:</p>
	<ul>
		<li><a href="#description">Définition de la liste</a>&#160;;</li>
		<li><a href="#sending">Diffusion/réception</a>&#160;;</li>
		<li><a href="#command">Droits d'accès</a>&#160;;</li>
		<li><a href="#archives">Archives</a>&#160;;</li>
		<li><a href="#bounces">Gestion des rapports de non-remise</a>&#160;;</li>
		<li><a href="#other">Divers</a>.</li>
	</ul>
	<p>Nous vous conseillons d'<strong>effectuer vos changements de configuration très progressivement</strong>&#160;: ainsi, si le résultat ne correspond pas à vos attentes, vous pouvez plus facilement revenir en arrière.</p>
	<p>Le module d'administration de la liste vous offre une <strong>multitude d'options pour configurer votre liste</strong>. Néanmoins, il se peut que ces options ne correspondent pas parfaitement à vos besoins. Pour remédier à ce problème, vous pouvez <strong>faire appel aux listmasters pour qu'ils créent de nouvelles options</strong> (dans la limite de leurs capacités)&#160;: accès et/ou droits restreints à certaines catégories de personnes en fonction de l'endroit d'où elles se connectent, du domaine auquel leur adresse e-mail appartient, du groupe d'utilisateur dont elles font partie, etc.</p>

	<h5><a name="description"></a>Définition de la liste</h5>
	<p>Dans la zone '<strong>Objet de la liste</strong>', vous pouvez modifier l'objet que vous aviez choisi lors de la création de la liste. Cet objet apparaît dans le bandeau bleu visible en haut de toutes les pages de la liste, ainsi que sur les pages d'index de listes (liste des listes, liste de vos abonnements, etc.) et dans la barre de titre du navigateur.</p>
	<p>Vous pouvez également modifier la '<strong>Visibilité de la liste</strong>'. Les différentes options disponibles sont&#160;:</p>
	<ul>
		<li>liste cachée sauf aux abonnés (conceal) - <em>option par défaut</em>&#160;;</li>
		<li>visible de l'intranet (intranet)&#160;;</li>
		<li>liste visible (noconceal)&#160;;</li>
		<li>visible des abonnés (private)&#160;;</li>
		<li>liste cachée même aux abonnés (secret)&#160;;</li>
		<li>abonné à nos grosses listes techniques (semipublic).</li>
	</ul>
	<p class="retraitita">Si vous souhaitez restreindre la visibilité de la liste suivant d'autres critères, faites-en la demande aux listmasters&#160;: il est probable qu'ils puissent créer une nouvelle option correspondant à vos besoins (exemple&#160;: liste visible uniquement par les membres d'un groupe d'utilisateurs, d'un domaine internet, etc.).</p>
	<p>Dans les zones 'Propriétaires' et 'Modérateurs', vous pouvez <strong>ajouter des propriétaires et des modérateurs</strong> à la liste ou <strong>modifier leurs informations</strong>&#160;:</p>
	<ul>
		<li>Pour chaque propriétaire/modérateur, vous devez obligatoirement saisir une <strong>adresse e-mail</strong> et un <strong>nom</strong>.</li>
		<li>Vous pouvez également ajouter les informations de votre choix dans le champ '<strong>Informations privées</strong>' (numéro de téléphone, fonction, etc.)&#160;; ces informations ne seront visibles que par les listmasters et les propriétaires de listes possédant un profil de type 'Privileged'.</li>
		<li>Vous pouvez modifier le <strong>mode de réception</strong> des messages (les seules options disponibles sur cette page sont 'mail' et 'nomail').</li>
		<li>Le paramètre 'Profil' n'est pas modifiable&#160;: le <strong>profil 'Privileged'</strong> est réservé au créateur de la liste (les autres propriétaires ont un profil du type 'Normal').</li>
	</ul>
	<p class="retraitita">Attention&#160;: le fait de devenir propriétaire ou modérateur d'une liste n'implique pas d'être abonné automatiquement à cette liste. Les propriétaires et modérateurs doivent donc faire la démarche de s'abonner à la liste.</p>
	<p>Pour <strong>supprimer des propriétaires/modérateurs</strong>, effacez le contenu des champs de texte relatifs au propriétaire/modérateur que vous souhaitez supprimer et cliquez sur le bouton 'Mise à jour'.</p>
	<p>Vous pouvez également modifier la <strong>catégorie</strong> de la liste ainsi que sa <strong>langue</strong>. Si vous changez la langue de la liste, tous les messages prédéfinis seront envoyés dans la langue choisie (attention&#160;: sous réserve que la traduction existe&#160;!).</p>
	<p>Vous ne pouvez pas modifier le <strong>domaine internet</strong> de la liste&#160;: seul les listmasters peuvent modifier ce paramètre.</p>
	<p><strong>ATTENTION&#160;: n'oubliez pas de cliquer sur 'Mise à jour'</strong> au bas de la page pour enregistrer tous vos changements.</p>

	<h5><a name="sending"></a>Diffusion/réception</h5>
	<p>À partir de cette page, vous pouvez tout d'abord <strong>décider qui peut envoyer des messages sur la liste</strong>.</p>
	<p>Dans la zone '<strong>Périodicité des compilations</strong>', vous pouvez définir à quel intervalle sont envoyées les compilations de messages (modes de réception Compilation et Résumé)&#160;: sélectionnez dans la liste tous les <strong>jours</strong> pour lesquels vous souhaitez que des compilations soient envoyées. Choisissez ensuite un <strong>horaire d'envoi</strong> des compilations (évitez de choisir un horaire compris entre 23h et minuit).</p>
	<p>Dans la liste '<strong>Options d'abonné disponibles</strong>', sélectionnez toutes les options d'abonné que vous souhaitez offrir à vos abonnés. Par défaut, toutes les options sont sélectionnées.</p>
	<p>La zone '<strong>Adresse de réponse</strong>' vous permet de définir les destinataires par défaut lors d'une réponse à un message envoyé sur la liste&#160;:</p>
	<ul>
		<li>Lorsque la valeur choisie est '<strong>All</strong>', la réponse est envoyée à l'<strong>expéditeur du message</strong> ET à la <strong>liste</strong>.</li>
		<li>Lorsque la valeur choisie est '<strong>List</strong>', la réponse est envoyée à la <strong>liste</strong>.<br />
		<p class="retraitita">Attention&#160;: cette option est à utiliser avec précaution&#160;! L'expérience montre que les abonnés ne vérifient pas toujours l'adresse à laquelle ils expédient leur réponse. Ils risquent donc d'envoyer des messages privés à l'ensemble des abonnés en pensant répondre uniquement à une personne...</p></li>
		<li>Lorsque la valeur choisie est '<strong>Other_email</strong>', la réponse est envoyée à une <strong>adresse prédéfinie</strong>. Si vous choisissez cette option, vous devez <strong>saisir une adresse e-mail dans le champ 'Autre adresse email'</strong>.</li>
		<li>Lorsque la valeur choisie est '<strong>Sender</strong>', la réponse est envoyée à l'<strong>expéditeur du message</strong>. C'est la valeur que nous vous conseillons de choisir.</li>
	</ul>
	<p>La liste déroulante '<strong>Respect du champ existant</strong>' vous permet de choisir le traitement du champ d'en-tête SMTP 'Reply-To' dans les messages entrants. L'option '<strong>Respect</strong>' préserve ce champ tandis que l'option '<strong>Forced</strong>' permet de l'écraser.</p>
	<p>Enfin, l'option '<strong>Marquage du sujet des messages</strong>' permet de choisir le <strong>texte qui sera inséré devant l'objet de tous les messages</strong> envoyés sur la liste&#160;: ceci permet aux abonnés de pouvoir trier leurs messages plus facilement, de leur appliquer des filtres de messages, etc. Par défaut, ce texte est constitué du <strong>nom de la liste entouré de crochets</strong> (les crochets sont rajoutés automatiquement par le système, il est inutile de les indiquer vous-même).</p>
	<p><strong>ATTENTION&#160;: n'oubliez pas de cliquer sur 'Mise à jour'</strong> au bas de la page pour enregistrer tous vos changements.</p>

	<h5><a name="command"></a>Droits d'accès</h5>
	<p>À partir de cette page, vous pouvez décider&#160;:</p>
	<ul>
		<li><strong>qui a accès aux informations sur la liste</strong>. Les options disponibles sont les suivantes&#160;:
		<ul>
			<li>pour tous (open) - <em>option par défaut</em>&#160;;</li>
			<li>réservé aux abonnés (private).</li>
		</ul></li>
		<li><strong>qui peut s'abonner à la liste</strong>. Les options disponibles sont les suivantes&#160;:
		<ul>
			<li>demande d'abonnement après confirmation (auth)&#160;;</li>
			<li>soumis à authentification (notification des proprios) (auth_notify)&#160;;</li>
			<li>soumis à authentification puis accord du proprio (auth_owner)&#160;;</li>
			<li>abonnement impossible (closed)&#160;;</li>
			<li>limité aux utilisateurs du domaine local (intranet)&#160;;</li>
			<li>usager du domaine local ou via autorisation du proprio (intranetorowner)&#160;;</li>
			<li>ouvert à tous sans authentification (open) - <em>option par défaut</em>&#160;;</li>
			<li>ouvert à tous, notification du propriétaire (open_notify)&#160;;</li>
			<li>n'importe qui pas de bienvenue (open_quiet)&#160;;</li>
			<li>ouvert à tous après autorisation du proprio (owner)&#160;;</li>
			<li>demande d'abonnement avec signature par certificat (smime)&#160;;</li>
			<li>ouvert à tous après autorisation du proprio ou avec signature par certificat (smimeorowner).<br />
			<span class="retraitita">Il est conseillé de toujours choisir une option comportant le paramètre 'auth'&#160;: ainsi, le système demandera confirmation par e-mail au futur abonné avant de l'abonner à la liste. Ceci permet d'éviter des abonnements avec une adresse e-mail invalide et assure que personne ne peut être abonné à la liste à son insu.</span></li></ul></li>
		<li><strong>qui peut se désabonner de la liste</strong>. Les options disponibles sont les suivantes&#160;:
		<ul>
			<li>possible après authentification (auth)&#160;;</li>
			<li>authentification demandée, notification du proprio (auth_notify)&#160;;</li>
			<li>impossible (closed)&#160;;</li>
			<li>autorisé à tous sans authentification (open) - <em>option par défaut</em>&#160;;</li>
			<li>pas d'authentification (notification au proprio) (open_notify)&#160;;</li>
			<li>soumis à autorisation du proprio (owner).<br />
			<p class="retraitita">Il est conseillé de toujours choisir une option comportant le paramètre 'auth'&#160;: ainsi, le système demandera confirmation par e-mail à l'abonné avant de le désabonner. Ceci permet d'éviter que des personnes malintentionnées ne désabonnent des abonnés à leur insu.</p></li>
		</ul></li>
		<li><strong>qui peut inviter une nouvelle personne à s'abonner à la liste</strong>. Les options disponibles sont les suivantes&#160;:
		<ul>
			<li>fermé (closed)&#160;;</li>
			<li>invitation par le proprio sans authentification (owner)&#160;;</li>
			<li>réservé aux abonnés (private) - <em>option par défaut</em>&#160;;</li>
			<li>public (public).</li>
		</ul></li>
		<li><strong>qui a accès à la liste des abonnés</strong>. Les options disponibles sont les suivantes&#160;:
		<ul>
			<li>pour personne (closed)&#160;;</li>
			<li>les abonnés ou le domaine local (intranet)&#160;;</li>
			<li>listmaster seulement (listmaster)&#160;;</li>
			<li>proprio de liste seulement (et listmaster) (owner) - <em>option par défaut</em>&#160;;</li>
			<li>réservé aux abonnés (private)&#160;;</li>
			<li>accessible à tous (public).<br />
			<p class="retraitita">Il est fortement déconseillé de rendre la liste des abonnés accessible à tous. L'option 'Réservé aux abonnés' peut être intéressante afin de permettre à ceux-ci de communiquer entre eux sans passer par la liste. Cette option est néanmoins à éviter dans le cas d'une liste de type annonce regroupant des abonnés sans aucun lien entre eux.</p></li>
		</ul></li>
	</ul>
	<p><a name="docsrights"></a>Sur cette page, vous pouvez également <strong>définir les droits relatifs à l'espace de stockage partagé</strong> (section 'Documents' de la liste, accessible via un lien dans le menu de gauche). On distingue les droits de consultation et les droits d'édition des documents&#160;: </p>
	<ul>
		<li>Pour les <strong>droits de consultation</strong>, les options suivantes sont disponibles&#160;:
		<ul>
			<li>limité aux abonnés ou aux utilisateurs du domaine local (intranet)&#160;;</li>
			<li>limité aux propriétaires de la liste (owner)&#160;;</li>
			<li>limité aux abonnés (private) - <em>option par défaut</em>&#160;;</li>
			<li>documents publics (public).</li>
		</ul></li>
		<li>Pour les <strong>droits d'édition</strong>, les options suivantes sont disponibles&#160;:
		<ul>
			<li>limité aux propriétaires de la liste (owner) - <em>option par défaut</em>&#160;;</li>
			<li>limité aux abonnés (private)&#160;;</li>
			<li>documents publics (public).</li>
		</ul></li>
	</ul>
	<p>La zone de texte '<strong>Quota</strong>' vous permet de définir une <strong>taille maximale à ne pas dépasser pour l'espace de stockage partagé</strong>. Cette taille ne représente pas la taille maximale d'<em class="caps">un</em> document publié dans l'espace de stockage partagé, mais bien celle de l'ensemble des documents publiés sur la liste. Elle est exprimée en kilo-octets. Lorsqu'un abonné tente de publier un fichier trop gros par rapport à l'espace restant, il reçoit un message d'erreur.</p>
	<p>Pour <strong>plus d'informations sur la gestion de l'espace de stockage partagé</strong> (organisation de l'espace de stockage partagé, modification des droits d'accès, etc.), reportez-vous à la section <a href="[% path_cgi %]/help/shared">Utiliser l'espace de stockage partagé</a> du Guide de l'utilisateur.</p>
	<p><strong>ATTENTION&#160;: n'oubliez pas de cliquer sur 'Mise à jour'</strong> au bas de la page pour enregistrer tous vos changements.</p>

	<h5><a name="archives"></a>Archives</h5>
	<p>À partir de cette page, vous pouvez <strong>décider qui peut accéder aux archives de la liste en ligne</strong> (messages envoyés consultables sur l'interface web du serveur de listes). Les différentes options disponibles sont&#160;:</p>
	<ul>
		<li>fermé (closed)&#160;;</li>
		<li>les abonnés ou le domaine local (intranet)&#160;;</li>
		<li>listmaster (listmaster)&#160;;</li>
		<li>proprio de liste (owner)&#160;;</li>
		<li>les abonnés seulement (private)&#160;;</li>
		<li>public (public).</li>
	</ul>
	<p>La zone de texte '<strong>Quota</strong>' vous permet de définir une <strong>taille maximale à ne pas dépasser pour les archives de messages</strong>. Cette taille est exprimée en kilo-octets. Les propriétaires reçoivent une notification lorsque les archives atteignent 95&#160;% de la taille autorisée. Si la taille maximale dévolue aux archives est atteinte, les messages ultérieurs ne sont pas archivés.<br />
	<p class="retraitita">Même si les messages cessent d'être archivés, il reste évidemment possible d'envoyer des messages sur la liste.</p></p>
	<p>Il est également possible d'<strong>accéder aux archives de la liste par e-mail</strong>, en envoyant à <strong>[% conf.email %]@[% conf.host %]</strong> la commande suivante&#160;: <span class="commande"><strong>get nomdelaliste log.anneemois</strong></span> (exemple&#160;: <em class="example">get cri-exemple log.200507</em>). L'abonné reçoit alors une compilation de tous les messages envoyés au cours du mois choisi. Cette compilation est envoyée en texte brut et contient des balises <acronym lang="en" xml:lang="en" title="HyperText Markup Language">HTML</acronym> à la place du formatage d'origine&#160;; elle comprend également des en-têtes détaillés pour chaque message. Le paramètre '<strong>Archives textuelles</strong>' vous permet de définir&#160;:</p>
	<ul>
		<li><strong>qui a le droit</strong> de se faire envoyer par e-mail les archives récapitulatives des messages de la liste&#160;;</li>
		<li>la <strong>périodicité de création de ces archives</strong>. Par exemple, si la périodicité est 'month', l'ensemble des messages envoyés sur la liste en un mois sera regroupé dans un message d'archives unique qui pourra être demandé par e-mail au serveur.</li>
	</ul>
	<p>Si ce paramètre n'est pas défini, la liste n'aura aucune archive consultable par e-mail. Attention&#160;: <strong>seuls les listmasters ont le pouvoir de modifier ce paramètre.</strong></p>
	<p>Il est possible d'envoyer des <strong>messages cryptés au format <acronym lang="en" xml:lang="en" title="Secure/Multipurpose Internet Mail Extensions">S/MIME</acronym></strong> sur la liste. L'option '<strong>Archivage des messages cryptés</strong>' vous permet de définir comment ces messages vont être archivés&#160;:</p>
	<ul>
		<li>L'option '<strong>Cleartext</strong>' archive le message sous sa forme cryptée originale.</li>
		<li>L'option '<strong>Decrypted</strong>' archive le message sans qu'il ne soit chiffré.</li>
		<li>L'option '<strong>Original</strong>' archive le message sous sa forme originale.</li>
	</ul>
	<p class="retraita">Cette option s'applique à la fois aux archives de messages textuelles et aux archives en ligne, ainsi qu'aux compilations de messages envoyées aux personnes qui ont souscrit leur abonnement avec le mode de réception Digest.</p>	
	<p><strong>ATTENTION&#160;: n'oubliez pas de cliquer sur 'Mise à jour'</strong> au bas de la page pour enregistrer tous vos changements.</p>

	<h5><a name="bounces"></a>Gestion des rapports de non-remise</h5>
	<p>Les &#171;&#160;<strong>bounces</strong>&#160;&#187; représentent les <strong>abonnés en erreur</strong>, c'est-à-dire les abonnés qui ne reçoivent pas les messages envoyés sur la liste. Les raisons peuvent être variées&#160;: adresses qui n'existent plus, adresses momentanément indisponibles au moment de l'envoi de messages, capacité de la boîte de réception atteinte, etc.</p>
	<p>La section '<strong>Gestion des bounces</strong>' définit deux seuils&#160;:</p>
	<ul>
		<li>Le <strong>seuil d'alerte</strong> indique le taux d'abonnés en erreur à partir duquel le propriétaire de la liste recevra une <strong>notification intitulée 'Taux de bounces trop élevé'</strong> l'invitant à supprimer de sa liste les abonnés en erreur.</li>
		<li>Le <strong>seuil d'interruption</strong> indique le taux d'erreurs à partir duquel <strong>la distribution des messages de la liste sera automatiquement interrompue</strong> jusqu'à régularisation de la situation (généralement via la suppression des abonnés en erreur).</li>
	</ul>
	<p><a name="bouncers"></a>Les sections '<strong>bouncers_level1</strong>' et '<strong>bouncers_level2</strong>' permettent d'effectuer automatiquement des actions en direction des abonnés en erreur. Vous pouvez définir&#160;:</p>
	<ul>
		<li>les <strong>plages de scores qui définissent les bouncers de niveau&#160;1 et de niveau&#160;2</strong>. Par défaut, les bouncers de niveau&#160;1 ont un score compris entre 45 et 74 et les bouncers de niveau&#160;2 ont un score compris entre 75 et 100&#160;;<br />
		<p class="retraitita">Le score est déterminé par le nombre, le type et la fréquence des erreurs. Si la période de réception des erreurs est trop courte ou s'il n'y a pas eu beaucoup d'erreurs, aucun score n'est affecté au bouncer.</p></li> 
		<li>l'<strong>action à effectuer en direction des abonnés concernés</strong>&#160;: aucune action, notification, suppression de la liste des abonnés&#160;;</li>
		<li>la <strong>personne à notifier</strong> lorsqu'une action est entreprise&#160;: personne, les propriétaires de la liste, les listmasters. La notification envoyée lorsqu'une action est effectuée en direction des abonnés en erreur comprend les adresses de tous les abonnés concernés ainsi que le détail de l'action effectuée.</li>
	</ul>
	<p><strong>Pour gérer les erreurs</strong> (annuler les erreurs pour certains abonnés, désabonner les abonnés en erreur, demander un rappel des abonnements, etc.), <strong>allez à la page</strong> '<a href="[% path_cgi %]/help/admin#manage_bounces">Erreurs</a>' du module d'administration de la liste.</p>
	<p><strong>ATTENTION&#160;: n'oubliez pas de cliquer sur 'Mise à jour'</strong> au bas de la page pour enregistrer tous vos changements.</p>

	<h5><a name="other"></a>Divers</h5>
	<p>L'option '<strong>Expire_task</strong>' vous permet de définir un <strong>délai d'expiration automatique des abonnements</strong> à la liste&#160;: à intervalles réguliers (exemple&#160;: une fois par an), les abonnés recevront un message leur demandant de renouveler leur abonnement à la liste. S'ils ne le renouvellent pas, ils seront automatiquement désabonnés. Cette procédure permet de s'assurer que toutes les personnes abonnées à la liste sont réellement concernées et intéressées.</p>
	<p>L'option '<strong>Remind_task</strong>' vous permet d'<strong>envoyer des rappels d'abonnements à intervalles réguliers</strong> à tous les abonnés de la liste.</p>
	<p>L'option '<strong>Méthode de protection des adresses contre les robots spammeurs</strong>' permet d'éviter que les adresses e-mail des abonnés ne soient récupérées par des robots à des fins de spam. Cette option est valable pour l'ensemble des pages relatives à la liste.</p>
	<p>Sur cette page, vous pouvez également voir des <strong>informations sur la dernière mise à jour de la liste</strong> (qui l'a effectuée et à quelle date) ainsi que sur le <strong>nombre de changements de configuration</strong> depuis la création de la liste.</p>
	<p><strong>ATTENTION&#160;: n'oubliez pas de cliquer sur 'Mise à jour'</strong> au bas de la page pour enregistrer tous vos changements.</p>
