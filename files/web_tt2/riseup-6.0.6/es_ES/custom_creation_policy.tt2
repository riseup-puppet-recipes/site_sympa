<h3>Qué ofrecemos</h3>
<ul>
<li><b>Mantenido por activistas</b>: Nuestros servicios se apoyan por completo en contribuciones de personas usuarias, no vigilamos tu conducta para vender tus hábitos de uso a anunciantes.</li>
<li><b>Entrega segura</b>: tenemos soporte para Entrega de correo por <a href='https://we.riseup.net/help/starttls'>StartTLS</a>. Si quien envía o recibe un correo electrónico usa un proveedor de correo electrónico seguro, la entrega se cifrará (nadie podrá leer los correos en el trayecto).</li>
<li><b>Sitio de internet seguro</b>: <a href='https://lists.riseup.net/'>lists.riseup.net</a> es accesible sólo por https (la s de seguro: todo viaja cifrado), y además no guardamos las direcciones IP de quienes lo visitan.</li>
<li><b>Almacenamiento cifrado</b>: Las bases de datos y archivos de cada lista se almacenan en sistemas de archivo cifrados.</li>
<li><b>Listas flexibles</b>: Brindamos soporte a listas desde privadas con unas cuantas suscripciones, hasta listas grandes con decenas de miles de suscripciones.</li>
</ul>

<h3>Tus responsabilidades</h3>
<ul>
<li><b>Consentimiento previo</b>: Cada dirección de correo que agregas a la lista, debe contar con el consentimiento para ser suscrita. Ser parte de una organización, solicitarlo en línea o en una reunión, o aprobación verbal, todo cuenta como consentimiento previo. Si tienes dudas, ¡no suscribas a nadie! Si recibimos quejas respecto a tu lista, será puesta en evaluación o será inhabilitada. Somos muy estrictos sobre esta política, porque cualquier abuso con las listas, puede resultar en que todas las listas sean afectadas al ser bloqueado el servidor entero.</li>
<li><b>Enfoque político</b>: Sólo hospedamos listas que estén enfocadas principalmente en el cambio social liberador. La lista debe ser usada para el activismo radical o progresista.</li>
<li><b>Responsabilidades de administración</b>: Tú, quien administra la lista, eres responsable por desuscribir gente, moderar la lista, eliminar suscripciones que rebotan los correos, cerrar listas en desuso, y responder a las quejas relacionadas con tu lista.</li>
<li><b>Donaciones</b>: Este servicio se sustenta por completo en la ayuda mutua. Por favor <a href='http://riseup.net/donate'>contribuye con lo que puedas</a>.</li>
</ul>

<h3>Consejos</h3>
<ul>
<li><b>¡No utilices acrónimos!</b>: La solicitud de tu lista será rechazada si sólo usas acrónimos. Existen millones de grupos y no podemos lidiar con todos los acrónimos para ellos.</li>
<li><b>Por favor, sé paciente</b>: Somos un colectivo manejado por personas voluntarias, puede tomarnos unos pocos días aprobar las solicitudes. Si tu acción es mañana, ya es demasiado tarde crear una lista en riseup para hacerle difusión.</li>
<li><b>Términos del servicio</b>: Al crear una lista, aceptas el <a href='http://help.riseup.net/tos'>Acuerdo de Términos del Servicio</a> de riseup.net.</li>
</ul>

<h3>Otros proveedores de listas para activistas</h3>
Por favor considera también utilizar los servicios de listas de correo de nuestros amigos:
<ul>
<li><a href='https://en.wiki.aktivix.org/Activix:EmailAndLists'>Activix</a> (Reino Unido)</li>
<li><a href='http://www.autistici.org/it/services/discussion/mailinglists.html'>Autistici</a> (Italia)</li>
<li><a href='http://resist.ca/'>Resist</a> (Canadá)</li>
</ul>
