<h3>Ce que nous fournissons</h3>
<ul>
<li><b>Fourni par des activistes</b>: Nos services tournent exclusivement grâce aux contributions des utilisateurs et utilisatrices, pas en analysant vos habitudes pour vendre vos clics à un publicitaire.</li>
<li><b>Livraison sécurisée</b>: Nous utilisons la technologie <a href='https://we.riseup.net/help/starttls'>StartTLS</a> pour la livraison des mails. Si un destinataire ou un émetteur utilise un fournisseur de mail sécurisé, alors la livraison des mails sera chiffrée.</li>
<li><b>Site web sécurisé</b>: <a href='https://lists.riseup.net/'>lists.riseup.net</a> n’est accessible que par https, et nous n’enregistrons pas les adresses IP des visiteurs.</li>
<li><b>Stockage chiffré</b>: Nos bases de données des listes et les archives sont enregistrées dans des systèmes de fichiers chiffrés.</li>
<li><b>Listes flexibles</b>: Nous acceptons toute taille de liste, de la liste privée avec quelques inscrit-e-s jusqu’aux grandes listes de dizaines de milliers d’inscrit-e-s.</li>
</ul>

<h3>Vos responsabilités</h3>
<ul>
<li><b>Accord initial</b>: Toutes les adresses que vous ajoutez à votre liste doivent vous avoir donné leur accord préalable avant d’être ajoutées. Être membre d’une organisation, s’inscrire en ligne ou lors d’une réunion publique, ou encore un accord verbal comptent comme un accord préalable. en cas de doute, n’inscrivez pas une adresse: Si nous recevons des plaintes au sujet de votre liste, elle sera mise à l’épreuve ou désactivée. Nous sommes très stricts sur ce point, parce que n’importe quel abus sur une liste peut entrainer le blocage de tout le serveur.</li>
<li><b>Focus politique</b>: Nous n’hébergeons que les listes qui sont d’abord orientées vers le changement social libertaire. La liste doit être utilisée pour le militantisme progressiste ou radical.</li>
<li><b>Responsabilités d’administration</b>: Vous, le propriétaire de la liste, êtes responsables de la désinscription des personnes, modération de la liste, suppression des adresses en erreur, fermeture des listes non utilisées et de la prise en charge des plaintes concernant votre liste.</li>
<li><b>Dons</b>: Ce service fonctionne uniquement grâce à notre assistance mutuelle. Merci de <a href='http://riseup.net/donate'>contribuer dans la mesure de vos moyens</a>.</li>
</ul>

<h3>Astuces</h3>
<ul>
<li><b>N’utilisez pas d’acronymes !</b> : Vos demandes de liste seront rejetées si vous utilisez uniquement des acronymes. Il y a des millions de groupes par ici et nous ne pouvons pas gérer autant d’acronymes.</li>
<li><b>Soyez patient</b>: Nous sommes un collectif organisé sur la base du bénévolat, et ça peut nous prendre quelques jours pour valider une demande. Si votre action se passe demain, il est déjà trop tard pour créer une liste riseup afin de la faire connaître.</li>
<li><b>conditions d’utilisation</b>: En créant une liste, vous êtes d’accord avec les <a href='http://help.riseup.net/tos'>conditions d’utilisation</a> de riseup.net.</li>
</ul>

<h3>Autres fournisseurs de liste</h3>
Vous pouvez aussi utiliser les services de liste de nos amis:
<ul>
<li><a href='https://en.wiki.aktivix.org/Activix:EmailAndLists'>Activix</a> (GB)</li>
<li><a href=''http://www.autistici.org/it/services/discussion/mailinglists.html'>Autistici</a> (Italie)</li>
<li><a href='http://resist.ca/'>Resist</a> (Canada)</li>
</ul>
