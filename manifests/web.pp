# Riseup's sympa web stuff css/icons/etc
class site_sympa::web {

  file {

    # html
    '/home/sympa/docroot/index.html':
      source => 'puppet:///modules/site_sympa/web/index.html',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/500.html':
      source => 'puppet:///modules/site_sympa/web/500.html',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    # robots.txt
    # right now we disallow everything, mainly due to past problems with
    #  1) the machine being overloaded 2) list admins being upset their
    #  archives were googlable. But robots.txt is only followed by polite
    #  web-crawlers anyway, I'm sure the feds still crawl. The best solution
    #  would be our plan to periodically inform list admins who have open
    #  archives. Until that is setup, we need to keep this locked down.
    '/home/sympa/docroot/robots.txt':
      source => 'puppet:///modules/site_sympa/web/robots.txt',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    # CSS
    '/home/sympa/docroot/cssx':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0755';

    '/home/sympa/docroot/cssx/riseup.css':
      source => 'puppet:///modules/site_sympa/web/css/riseup.css',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/cssx/fullPage.css':
      source => 'puppet:///modules/site_sympa/web/css/fullPage.css',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/cssx/print-preview.css':
      source => 'puppet:///modules/site_sympa/web/css/print-preview.css',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/cssx/print.css':
      source => 'puppet:///modules/site_sympa/web/css/print.css',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/cssx/style.css':
      source => 'puppet:///modules/site_sympa/web/css/style.css',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    # images referenced by the css
    # FIXME: put them in cssx for now until we sort out the static-content dir
    '/home/sympa/docroot/cssx/bg-top.png':
      source => 'puppet:///modules/site_sympa/web/images/bg-top.png',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/cssx/fadey-bg.png':
      source => 'puppet:///modules/site_sympa/web/images/fadey-bg.png',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/cssx/grey9.png':
      source => 'puppet:///modules/site_sympa/web/images/grey9.png',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/cssx/riseup-8.png':
      source => 'puppet:///modules/site_sympa/web/images/riseup-8.png',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/cssx/white-shade-top.png':
      source => 'puppet:///modules/site_sympa/web/images/white-shade-top.png',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/icons':
      ensure => link,
      target => '/home/sympa/static_content/icons';

    # symlink img -> icons -- we used to use img, but shouldn't be
    # anymore, this is a transition symlink
    # the list archives are still using this, until they are all regenerated
    # we need to keep it around
    '/home/sympa/docroot/img':
      ensure => link,
      target => '/home/sympa/docroot/icons';

    # favicons come from the main puppet repo
    '/home/sympa/docroot/favicon.ico':
      source => 'puppet:///modules/riseup/favicon.ico',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    # We deliver the png version twice so we don't have to modify templates
    '/home/sympa/docroot/favicon.png':
      source => 'puppet:///modules/riseup/favicon.png',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/icons/favicon_sympa.png':
      source => 'puppet:///modules/riseup/favicon.png',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/inc':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0755';

    # These files are needed by the updatedirectory.rb script
    '/home/sympa/docroot/inc/header.html':
      source => 'puppet:///modules/site_sympa/web/inc/header.html',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/docroot/inc/footer.html':
      source => 'puppet:///modules/site_sympa/web/inc/footer.html',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

  }

  cron {
    # generates the topic directory on the webserver
    # FIXME: we should have stuff under file that ensures the directory needed
    #  for this script exists with the right ownership and permissions. Also
    #  the script itself should do some error checking
    'updatedirectory':
      command     => '/home/sympa/tools/updatedirectory.rb',
      user        => sympa,
      environment => 'MAILTO=root@riseup.net',
      minute      => 0,
      hour        => 3,
      require     => Service['cron'];
  }

  # rotate more frequently because we don't want the logs and they end up large
  augeas { 'logrotate_apache':
    context => '/files/etc/logrotate.d/apache2/rule',
    changes => [ 'set rotate 4', 'set schedule daily' ]
  }
}
