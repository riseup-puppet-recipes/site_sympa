class site_sympa::mail_tt2 (
  $version
) {

  file {

    # riseup's mail templates. We only deliver things that are different
    #  than the upstream default directory
    '/home/sympa/etc/mail_tt2':
      ensure  => directory,
      source  => "puppet:///modules/site_sympa/mail_tt2/riseup-${version}",
      recurse => true,
      require => File['/home/sympa/etc'],
      owner   => sympa,
      group   => sympa,
      mode    => '0755';
  }

}
