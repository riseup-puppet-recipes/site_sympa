class site_sympa::web_tt2 (
  $version
) {

  file {

    # riseup's web templates. We only deliver things that are different
    #  than the upstream default directory
    '/home/sympa/etc/web_tt2':
      ensure  => directory,
      source  => "puppet:///modules/site_sympa/web_tt2/riseup-${version}",
      recurse => true,
      require => File['/home/sympa/etc'],
      owner   => sympa,
      group   => sympa,
      mode    => '0755';

    # allow sympa read access to the sympa db for stats cronjobs
    '/home/sympa/.my.cnf':
      content => template('site_sympa/my.cnf.erb'),
      owner   => sympa,
      group   => sympa,
      mode    => '0640';

  }

  cron {
    # FIXME: move the scripts into to puppet rather than tools
    # generates stats for the sympa admin page
    # needs db access, output goes on the web server
    'counts':
      command     => '/home/sympa/tools/stats/counts -h>/home/sympa/etc/web_tt2/counts.tt2',
      user        => sympa,
      environment => 'MAILTO=root@riseup.net',
      minute      => 0,
      hour        => 4,
      require     => Service['cron'];
    # generates list of top lists for the sympa admin page
    # needs db access, output goes on the web server
    'toplists':
      command     => '/home/sympa/tools/stats/toplists -h>/home/sympa/etc/web_tt2/toplists.tt2',
      user        => sympa,
      environment => 'MAILTO=root@riseup.net',
      minute      => 5,
      hour        => 4,
      require     => Service['cron'];
    # generates list of top domains for the sympa admin page
    # needs db access, output goes on the web server
    'topdomains':
      command     => '/home/sympa/tools/stats/topdomains -h>/home/sympa/etc/web_tt2/topdomains.tt2',
      user        => sympa,
      environment => 'MAILTO=root@riseup.net',
      minute      => 10,
      hour        => 4,
      require     => Service['cron'];
    # generates list of top languages for the sympa admin page
    # needs db access, output goes on the web server
    'toplangs':
      command     => '/home/sympa/tools/stats/toplangs -h>/home/sympa/etc/web_tt2/toplangs.tt2',
      user        => sympa,
      environment => 'MAILTO=root@riseup.net',
      minute      => 15,
      hour        => 4,
      require     => Service['cron'];
  }
}
