class site_sympa::db {

  # needed for sympa-bad-domains script
  include perl::extensions::email_valid

  file {

    ## cronjob files and resources
    '/home/sympa/cron':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0755';

    '/home/sympa/cron/sympa-bad-domains':
      source  => 'puppet:///modules/site_sympa/cron/sympa-bad-domains',
      owner   => root,
      group   => sympa,
      mode    => '0750',
      require => File['/home/sympa/cron'];

    # currently sympa-bad-domains needs a newer version of Parallel::ForkManager
    # than is in Debian, so we deliver a copy. Once it's in debian we can add
    # it to the package list instead and depend on that.
    # Note this is a minor security issue
    #   https://labs.riseup.net/code/issues/3997
    '/home/sympa/cron/Parallel':
      ensure  => directory,
      owner   => sympa,
      group   => sympa,
      mode    => '0755',
      require => File['/home/sympa/cron'];

    '/home/sympa/cron/Parallel/ForkManager.pm':
      source  => 'puppet:///modules/site_sympa/cron/Parallel/ForkManager.pm',
      owner   => root,
      group   => sympa,
      mode    => '0755',
      require => File['/home/sympa/cron/Parallel'];

    # directory where the long term results are stored
    '/var/log/sympa/bad-domains':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0750';
  }

  cron {
    # bad domains checker tools
    'sympa-bad-domains':
      command     => '/home/sympa/cron/sympa-bad-domains',
      user        => sympa,
      environment => 'MAILTO=root@riseup.net',
      minute      => 0,
      hour        => 4,
      monthday    => 3,
      require     => Service['cron'];

    'sympa-persistent-bad-domains':
      command     => '/home/sympa/tools/bad-domains/sympa-persistent-bad-domains',
      user        => sympa,
      environment => 'MAILTO=root@riseup.net',
      minute      => 0,
      hour        => 4,
      monthday    => 5,
      require     => Service['cron'];

    # automatically detect lists bulk-adding, auto-close if bad, report
    'bulkdetect':
      command     => '/home/sympa/tools/bulkdetect -c',
      user        => sympa,
      environment => 'MAILTO=root@riseup.net',
      minute      => 0,
      hour        => 5,
      require     => Service['cron'];
  }
}
