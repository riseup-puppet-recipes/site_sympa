# make sure the footer is added on every list that doesn't have one
class site_sympa::addfooter {

  cron { 'addfooter':
    command     => '/home/sympa/tools/addfooter',
    user        => sympa,
    environment => 'MAILTO=root@riseup.net',
    minute      => 0,
    hour        => 15,
    require     => Service['cron']
  }
}
