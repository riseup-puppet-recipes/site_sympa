class site_sympa::bannedcheck {

  include perl::extensions::monitoring_plugin

  file {
    # FIXME: we could move the banned checker here from the other sympa git repo
    # since the intent is for this module to be public

    # FIXME: replace nrpe with check-mk, this removed file might have
    #  interesting settings for check-mk
    '/etc/nagios/nrpe.d/sympa_banned.cfg':
      ensure => absent;

    '/usr/local/lib/nagios/plugins/check_sympa_banned':
      source  => 'puppet:///modules/site_sympa/banned/check_sympa_banned',
      owner   => root,
      group   => nagios,
      mode    => '0750',
      require => File['/usr/local/lib/nagios/plugins'];

    # FIXME:right now these things are in the main puppet repo (which is good)
    # but maybe we need to be better about how we reference it?

    # allow nagios read access to the sympa db for the above check
    '/var/lib/nagios/.my.cnf':
      content => template('site_sympa/my.cnf.erb'),
      owner   => root,
      group   => nagios,
      mode    => '0640';

    # resources for the banned user checker
    '/home/sympa/etc/banned':
      ensure  => directory,
      source  => 'puppet:///modules/site_sympa/banned',
      recurse => true,
      owner   => sympa,
      group   => sympa,
      mode    => '0755';
  }

  # FIXME: replace nrpe with check-mk
  #nagios::service {
  #  'check_sympa_banned':
  #    check_command => 'check_nrpe_timeout_1arg!60!check_sympa_banned';
  #}
}
