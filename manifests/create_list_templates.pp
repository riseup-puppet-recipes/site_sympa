class site_sympa::create_list_templates (
 $version
){

  file {

    '/home/sympa/etc/create_list_templates':
      ensure  => directory,
      source  => "puppet:///modules/site_sympa/create_list_templates/riseup-${version}",
      recurse => true,
      owner   => root,
      group   => root,
      mode    => '0755';

    # we don't want any of the upstream defaults
    '/home/sympa/default/create_list_templates':
      ensure  => absent,
      recurse => true,
      force   => true;
  }

}
