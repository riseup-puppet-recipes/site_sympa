class site_sympa (
  $version = '6.1.22'
) {

  $use_nagios = hiera('use_nagios')
  $use_munin = hiera('use_munin')

  include site_postfix::listserver
  include site_sympa::web
  class { 'site_sympa::scenari': version => $version }
  class { 'site_sympa::create_list_templates': version => $version }
  include site_sympa::db
  include site_sympa::addfooter

  # we only run the banned user checker on whimbrel
  case $hostname {
    'stilt': {
      include site_apache
    }
    'whimbrel': {
      include site_sympa::bannedcheck
      class { 'apache':
        default_vhost     => false,
        default_ssl_vhost => false,
        mpm_module        => 'event',
        manage_user       => false,
        manage_group      => false,
        user              => 'sympa',
        group             => 'sympa'
      }
      
      apache::listen { '80': }
      apache::listen { '443': }
      
      apache::mod { 'removeip': package => 'libapache2-mod-removeip' }
      # needed because doing a apache::vhost::custom
      include apache::mod::ssl

      # make sure server-status is not available to tor
      include site_apache::tor

    }
  }

  # dir module is needed for the topics /directory/ index.html stuff
  include apache::mod::dir
  include apache::mod::rewrite
  include apache::mod::headers

  apache::vhost::custom {
    'lists.riseup.net':
      content => template('site_sympa/apache2/lists.riseup.net');
  }

  class {'site_sympa::mail_tt2': version => $version}
  class {'site_sympa::web_tt2': version => $version}

  # we set this since we don't want the debian package installed
  class { 'sympa':
            upstream => true,
            munin    => $use_munin,
            nagios   => $use_nagios
  }

  package {
    ## these are riseup specific

    # needed for sympa build, tools
    [ gettext, git-core ]:
      ensure => installed;

    # php5-cli needed for ecology, maybe other stuff?
    # libdata-password-perl needed for password checking
    [ 'php5-cli', 'libdata-password-perl' ]:
      ensure => installed;

  }

  ## move these to the shared-module somehow
  # do locale generation

  group { 'sympa':
    ensure    => present,
    name      => sympa,
    gid       => 1000,
    allowdupe => false
  }

  user {
    'sympa':
      ensure    => present,
      uid       => 1000,
      gid       => 1000,
      comment   => 'Sympa user',
      home      => '/home/sympa',
      shell     => '/bin/bash',
      groups    => nestd,
      password  => '!',
      allowdupe => false,
      require   => Group['sympa'];
  }

  riseup_common::locales {
    $::fqdn: type => 'lists';
  }

  # generic files, these will go to the generic module
  file {
    '/etc/sympa':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0755';

    # note sympa.conf is in the 'sympa' module
    '/etc/sympa/wwsympa.conf':
      source  => 'puppet:///modules/site_sympa/wwsympa.conf',
      owner   => sympa,
      group   => sympa,
      mode    => '0640',
      require => File['/etc/sympa'];

    '/home/sympa':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0755';

    '/home/sympa/docroot':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0755';

    '/home/sympa/docroot/maintenance.html.disabled':
      source => 'puppet:///modules/site_sympa/maintenance.html.disabled',
      owner  => sympa,
      group  => sympa,
      mode   => '0644';

    '/home/sympa/etc':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0755';

    '/home/sympa/etc/topics.conf':
      source  => 'puppet:///modules/site_sympa/topics.conf',
      owner   => root,
      group   => sympa,
      mode    => '0644',
      require => File['/home/sympa/etc'];

    '/home/sympa/etc/edit_list.conf':
      source  => 'puppet:///modules/site_sympa/edit_list.conf',
      owner   => root,
      group   => sympa,
      mode    => '0644',
      require => File['/home/sympa/etc'];

    '/home/sympa/etc/mhonarc-ressources.tt2':
      source  => 'puppet:///modules/site_sympa/mhonarc-ressources.tt2',
      owner   => root,
      group   => sympa,
      mode    => '0644',
      require => File['/home/sympa/etc'];

    '/etc/apache2/envvars':
      source => 'puppet:///modules/site_sympa/envvars',
      owner  => root,
      group  => root,
      mode   => '0644';

    '/var/lock/apache2':
      owner => sympa,
      group => root,
      mode  => '0755';

    '/var/lock/subsys':
      owner => sympa,
      group => sympa,
      mode  => '0755';

    '/var/lib/apache2/fcgid/sock':
      owner => sympa,
      group => root,
      mode  => '0755';

  }

  # riseup specific files
  file {
    # this is setup so the nestd cronjob can push the exported mail
    # users to the export user which is where the list called
    # 'mailusers' will pick up the file to include the addresses as
    # subscribers. the 'newsletter' list will then pick up those
    # addresses
    '/home/nestd/.ssh':
      ensure => directory,
      owner  => nestd,
      group  => nestd,
      mode   => '0700';

    # FIXME: this key needs to be stored in the main puppet repo
    '/home/nestd/.ssh/id_rsa':
      source  => 'puppet:///modules/riseup_common/keys/nestd_id_rsa',
      owner   => nestd,
      group   => nestd,
      mode    => '0600',
      require => File['/home/nestd/.ssh'];

    #
    '/var/log/sympa/biglists':
      ensure => directory,
      owner  => sympa,
      group  => sympa,
      mode   => '0750';
  }

  # have check_mk gather info about the user list file we pull from nest
  class { 'site_check_mk::agent::fileinfo': content => '/home/nestd/email_addresses' }

  case $::lsbdistcodename {
    'squeeze': { $munin_location = '/var/lib/munin/.my.cnf' }
    default: {
      $munin_location = '/var/lib/munin-node/.my.cnf'
      file { '/var/lib/munin': ensure => link, target => '/var/lib/munin-node' }
    }
  }
  # don't allow us to fuck things up
  file {
    '/home/sympa/expl/newsletter':
      ensure => directory,
      owner  => root,
      group  => root,
      mode   => '0755';

    '/home/sympa/expl/newsletter/config':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644';

    '/home/sympa/expl/listadmins':
      ensure => directory,
      owner  => root,
      group  => root,
      mode   => '0755';

    '/home/sympa/expl/listadmins/config':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644';

    '/home/sympa/expl/mailusers':
      ensure => directory,
      owner  => root,
      group  => root,
      mode   => '0755';

    '/home/sympa/expl/mailusers/config':
      ensure => file,
      owner  => root,
      group  => root,
      mode   => '0644';
  }

  file {
    # allow munin plugins (in sympa module) read access to the sympa db
    # this file comes from the main puppet repo since it contains credentials
    $munin_location:
      content => template('site_sympa/my.cnf.erb'),
      owner   => root,
      group   => munin,
      mode    => '0640';
  }
}
