class site_sympa::scenari (
  $version
){

  file {

    # resources for the banned user checker
    '/home/sympa/etc/scenari':
      ensure  => directory,
      source  => "puppet:///modules/site_sympa/scenari/riseup-${version}",
      recurse => true,
      purge   => true,
      require => File['/home/sympa/etc'],
      owner   => root,
      group   => root,
      mode    => '0755';

    ## eventually we'll be able to clean up the default/scenari but
    ##  we still need them in place until there are no lists using them
    # we don't want any of the upstream defaults
    '/home/sympa/default/scenari':
      ensure  => absent,
      recurse => true,
      force   => true;

  }
}
